<?php

function partnerlist_shortcode_func( $atts ) {

	$a = shortcode_atts( array(
		'type'     => 'list',
		'group'    => 'all',
		'category' => 'all',
		'size'     => 'medium',
		'links'    => 'true',
		'controls' => 'hide',
		'title'    => 'show',
		'image'    => 'show',
		'hidecat'  => NULL,
		'catdesc'  => 'hide',
		'height'   => 180,
		'spacing'  => 0,
		'orderby'  => 'id',
	), $atts );

	if ( $a['group'] == "all" OR $a['group'] == 0 ) {
		$termArr = get_terms('partners-groups');
		foreach($termArr as $key => $termA) {
			$a['group'][$key] = $termA->term_id;
			print_r($a);
		}
	}

	if ( $a['category'] == "all" OR $a['category'] == 0 ) {
		$custom_terms = get_terms('partners-categories');
	} else {
		$custom_terms[0] = get_term_by( 'term_id', $a['category'], 'partners-categories' );
	}

	if ($a['hidecat']) {
		foreach($custom_terms as $key => $cstmtrm) {
			if ($a['hidecat'] == $cstmtrm->term_id) {
				unset($custom_terms[$key]);
			}
		}
	}

	$getviewtype = partnerlist_views_get($a['type']);

	$dir = plugin_dir_path( __FILE__ );
	require $dir . "../views/" . $getviewtype['slug'] . ".php";

}
add_shortcode( 'partner-list', 'partnerlist_shortcode_func' );
