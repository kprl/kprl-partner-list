<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					echo the_title( '<h1>', '</h1>' );
					the_post_thumbnail();

					$meta = get_post_meta( get_the_ID() );



					if ( isset( $meta['partnerlist_partners_externlink'] ) ) {
						$link = $meta['partnerlist_partners_externlink'][0];
						echo "<p style='margin-top: 40px;'>Extern länk: <a href='" . $link . "' target='_blank'>" . $link . "</a></p>";
					}

					if ( isset( $meta['partnerlist_partners_downloads'] ) ) {
						$antDownloads = $meta['partnerlist_partners_downloads'][0];
					} else {
						$antDownloads = 0;
					}

					$i = 0;
					while ($i < $antDownloads) {
						$fil = "partnerlist_partners_downloads_" . $i . "_fil";
						$txt = "partnerlist_partners_downloads_" . $i . "_kommentar";

						$id	= $meta[$fil][0];
						$filename = get_attached_file( $id );
						$filetype = wp_check_filetype( $filename );

						$arr[$i]['id'] 				= $id;
						$arr[$i]['uri']				= $filename;
						$arr[$i]['url']				= wp_get_attachment_url( $id );
						$arr[$i]['name']			= basename( $filename );
						$arr[$i]['ext']				= $filetype['ext'];
						$arr[$i]['type']			= $filetype['type'];
						$arr[$i]['size'] 			= size_format( filesize( $filename ) );
						$arr[$i]['kommentar'] = $meta[$txt][0];
						$i++;
					}

					foreach ($arr as $key => $value) {
						echo "<h3>" . get_the_title($value['id']) . "</h3>";
						echo "<ul>";
							echo "<li>Filtyp: <b>" . $value['ext'] . "</b> (" . $value['type'] . ")</li>";
							echo "<li>Filstorlek: <b>" . $value['size'] . "</b></li>";
							echo "<li>Ladda ner: <b><a href='" . $value['url'] . "' download>" . $value['name'] . "</a></b></li>";
						echo "</ul>";
					}

				endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
