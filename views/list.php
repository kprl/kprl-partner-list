<?php

	foreach($custom_terms as $custom_term) {

		$args = array(
			'post_type' 	 => 'partners',
			'showposts' 	 => -1,
			'tax_query' 	 => array(
				'relation' 	 => 'AND',
				array(
					'taxonomy' => 'partners-categories',
					'field'    => 'term_id',
					'terms'    => $custom_term->term_id,
				),
				array(
					'taxonomy' => 'partners-groups',
					'field'    => 'term_id',
					'terms'    => $a['group'],
				),
			),
			'orderby' 		 => $a['orderby'],
		);

		$loop = new WP_Query($args);
		if($loop->have_posts()) {

			echo '<div id="partner-list-category-' . $custom_term->term_id . '" class="partner-list-category partner-list-view-list">';

			echo '<ul>';

			echo '<h1 class="partner-list-category-title">' . $custom_term->name . '</h1>';

			if ( array_key_exists( 'catdesc', $a ) ) {
				if ($a['catdesc'] == 'show') {
					echo "<p>" . $custom_term->description . "</p>";
				}
			}

			while($loop->have_posts()) : $loop->the_post();

				echo '<li id="partner-list-partner-' . get_the_ID() . '" class="partner-list-category-partner">';

				$pl_meta_value = get_post_meta(get_the_ID());
				$the_link = $pl_meta_value['partnerlist_partners_externlink'][0];

				if ( $a['title'] == "show") {

					if ( $a['links'] == "true" AND $the_link) {

						echo '<a href="' . $the_link . '" target="_blank"><h2>' . get_the_title() . ' <small>' . get_the_excerpt() . '</small></h2></a>';

					} else {

						echo '<h2>' . get_the_title() . ' <small>' . get_the_excerpt() . '</small></h2>';

					}

				}

				if ( $a['links'] == "true" AND $the_link) {

					echo '<a href="' . $the_link . '" target="_blank">';
					if ( has_post_thumbnail() ) {
						the_post_thumbnail( $a['size'], array('class' => 'img-responsive' ));
					}
					echo '</a>';

				} else {

					if ( has_post_thumbnail() ) {
						the_post_thumbnail( $a['size'], array('class' => 'img-responsive' ));
					}

				}

				echo '</li>';

			endwhile;

			echo '</ul>';

			echo '</div>';

		}
		wp_reset_query();
	}
