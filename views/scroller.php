<?php

	echo '<div id="partner-list-category-scroller" class="scroller slide" data-ride="scroller">';

		wp_enqueue_style( 'partner-list-style', plugins_url() . '/kprl-partner-list/assets/partner-list-style.css' );

		$i = 0;

		foreach($custom_terms as $custom_term) {

			$args = array(
				'post_type' 	 => 'partners',
				'showposts' 	 => -1,
				'tax_query' 	 => array(
					'relation' 	 => 'AND',
					array(
						'taxonomy' => 'partners-categories',
						'field'    => 'term_id',
						'terms'    => $custom_term->term_id,
					),
					array(
						'taxonomy' => 'partners-groups',
						'field'    => 'term_id',
						'terms'    => $a['group'],
					),
				),
				'orderby' 		 => $a['orderby'],
			);

			$loop = new WP_Query($args);

			if($loop->have_posts()) {

				echo '<div id="scroller" style="width: 100%; height: ' . $a['height'] . 'px; margin: 0 auto;">';
					echo '<div class="innerScrollArea">';
						echo '<ul>';

							while($loop->have_posts()) : $loop->the_post();

								$i++;

								echo '<li>';

									$pl_meta_value = get_post_meta(get_the_ID());
									if (isset($pl_meta_value['partnerlist_partners_externlink'][0])) {
										$the_link = $pl_meta_value['partnerlist_partners_externlink'][0];
									} else {
										$the_link = "";
									}

									if ( $a['links'] == "true" AND $the_link) { echo '<a href="' . $the_link . '" target="_blank">'; }

									if ($a['title'] == 'show') { echo '<h2>' . get_the_title() . ' <small>' . get_the_excerpt() . '</small></h2>'; }
									if ( has_post_thumbnail() ) {

										preg_match_all('/(height)=("[^"]*")/i', get_the_post_thumbnail( get_the_ID(), $a['size'] ), $img);
										$imgheight = str_replace('"', '', $img[2][0]);
										if ( $imgheight < $a['height'] ) {
											$margintop = $a['height'] - $imgheight;
											$margintop = $margintop / 2;
										} else {
											$margintop = 0;
										}

										the_post_thumbnail( $a['size'], array( 'style' => 'margin-right: ' . $a['spacing'] . 'px;' ) );
									}

									if ( $a['links'] == "true" AND $the_link) { echo '</a>'; }

								echo '</li>';

							endwhile;

						echo '</ul>';
					echo '</div>';
				echo '</div>';

			}
			wp_reset_query();
		}

		wp_enqueue_script( 'partner-list-scroller', plugins_url() . '/kprl-partner-list/assets/partner-list-scroller.js', array(), '1.0.0', true );

echo '</div>';
