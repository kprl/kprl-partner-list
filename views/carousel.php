<?php
	wp_enqueue_style( 'partner-list-carousel-css', plugins_url() . '/kprl-partner-list/assets/bootstrap.css', array(), '1.1', 'all');
	wp_enqueue_script( 'partner-list-carousel-js', plugins_url() . '/kprl-partner-list/assets/bootstrap.min.js', array(), '1.0.0', true );

echo '<div id="partner-list-category-carousel" class="carousel slide" data-ride="carousel">';

	echo '<div class="carousel-inner" role="listbox" style="height: ' . $a['height'] . 'px">';

	$i = 0;

	foreach($custom_terms as $custom_term) {

		$args = array(
			'post_type' 	 => 'partners',
			'showposts' 	 => -1,
			'tax_query' 	 => array(
				'relation' 	 => 'AND',
				array(
					'taxonomy' => 'partners-categories',
					'field'    => 'term_id',
					'terms'    => $custom_term->term_id,
				),
				array(
					'taxonomy' => 'partners-groups',
					'field'    => 'term_id',
					'terms'    => $a['group'],
				),
			),
			'orderby' 		 => $a['orderby'],
		);

		$loop = new WP_Query($args);

		if($loop->have_posts()) {

			while($loop->have_posts()) : $loop->the_post();

				$i++;

				echo "<div class='item"; if ($i == 1) { echo " active"; } echo "'>";

				$pl_meta_value = get_post_meta(get_the_ID());
				if (isset($pl_meta_value['partnerlist_partners_externlink'][0])) {
					$the_link = $pl_meta_value['partnerlist_partners_externlink'][0];
				} else {
					$the_link = "";
				}

				if ( $a['links'] == "true" AND $the_link) { echo '<a href="' . $the_link . '" target="_blank">'; }

				if ($a['title'] == 'show') { echo '<h2>' . get_the_title() . ' <small>' . get_the_excerpt() . '</small></h2>'; }
				if ( has_post_thumbnail() ) {

					preg_match_all('/(height)=("[^"]*")/i', get_the_post_thumbnail( get_the_ID(), $a['size'] ), $img);
					$imgheight = str_replace('"', '', $img[2][0]);
					if ( $imgheight < $a['height'] ) {
						$margintop = $a['height'] - $imgheight;
						$margintop = $margintop / 2;
					} else {
						$margintop = 0;
					}

					the_post_thumbnail( $a['size'], array('style' => 'max-height: ' . $a['height'] . 'px; width: auto; margin: ' . $margintop . 'px auto auto auto;' , 'class' => 'img-responsive' ));
				}

				if ( $a['links'] == "true" AND $the_link) { echo '</a>'; }

				echo "</div>";

			endwhile;

		}
		wp_reset_query();
	}

		?>

			</div>

			<?php if ($a['controls'] == 'show') { ?>

				<!-- Controls -->
				<a class="left carousel-control" href="#partner-list-category-<?php echo $postId; ?>-carousel" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#partner-list-category-<?php echo $postId; ?>-carousel" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>

			<?php }
echo '</div>';
