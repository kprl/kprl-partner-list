<?php
/**
 * Plugin Name: kprl Partner list
 * Plugin URI: https://bitbucket.org/kprl/kprl-partner-list
 * Bitbucket Plugin URI: https://bitbucket.org/kprl/kprl-partner-list
 * Description: Functionality for managing, listing and rotating sponsors and partners
 * Version: 1.2.1
 * Author: Karl Lettenström
 * Author URI: http://kprl.se
 * Text Domain: kprl-partner-list
 * Domain Path: Optional. Plugin's relative directory path to .mo files. Example: /locale/
 * Network: Optional. Whether the plugin can only be activated network wide. Example: true
 * License: GPL2
 */

/*  Copyright 2018 Karl Lettenström (email : karl@kprl.se)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

	//core-files
	include 'core/functions.php';
	include 'core/options.php';
	include 'core/acf_fieldgroups.php';

	//custom post types
	include 'cpt/partners.php';

	//taxonomies
	include 'taxonomies/categories.php';
	include 'taxonomies/groups.php';

	//views
	// partnerlist_views_add( ID, "NAME", "DESCRIPTION" );
	partnerlist_views_add( 1, "Grid", "Visa som rutnätsmönster" );
	partnerlist_views_add( 2, "Carousel", "Visa som carousel" );
	partnerlist_views_add( 3, "List", "Visa som lista" );
	partnerlist_views_add( 4, "Scroller", "Visa som scroller" );

	//widgets
	include 'widget/partner-list.php';

	//shortcodes
	include 'shortcodes/partner-list.php';
