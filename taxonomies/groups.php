<?php
	
add_action( 'init', 'partners_groups_taxonomies', 0 );
function partners_groups_taxonomies() {
	
	$labels = array(
		'name'              => _x( 'Partnersgrupper', 'taxonomy general name' ),
		'singular_name'     => _x( 'Partnersgrupp', 'taxonomy singular name' ),
		'search_items'      => __( 'Sök partnersgrupp' ),
		'all_items'         => __( 'Alla partnersgrupper' ),
		'parent_item'       => __( 'Föräldragrupp' ),
		'parent_item_colon' => __( 'Föräldragrupp:' ),
		'edit_item'         => __( 'redigera partnersgrupp' ),
		'update_item'       => __( 'Uppdatera partnersgrupp' ),
		'add_new_item'      => __( 'Lägg till ny partnersgrupp' ),
		'new_item_name'     => __( 'Ny partnersgrupp' ),
		'menu_name'         => __( 'Partnersgrupper' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'partners-groups' ),
		'public'            => false,
	);

	register_taxonomy( 'partners-groups', 'partners', $args );
}