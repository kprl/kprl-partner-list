<?php
	
add_action( 'init', 'partners_categories_taxonomies', 0 );
function partners_categories_taxonomies() {
	
	$labels = array(
		'name'              => _x( 'Partnerskategorier', 'taxonomy general name' ),
		'singular_name'     => _x( 'Partnerskategori', 'taxonomy singular name' ),
		'search_items'      => __( 'Sök partnerskategori' ),
		'all_items'         => __( 'Alla partnerskategorier' ),
		'parent_item'       => __( 'Föräldrakategori' ),
		'parent_item_colon' => __( 'Föräldrakategori:' ),
		'edit_item'         => __( 'redigera partnerskategori' ),
		'update_item'       => __( 'Uppdatera partnerskategori' ),
		'add_new_item'      => __( 'Lägg till ny partnerskategori' ),
		'new_item_name'     => __( 'Ny partnerskategori' ),
		'menu_name'         => __( 'Partnerskategorier' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'partners-categories' ),
		'public'            => false,
	);

	register_taxonomy( 'partners-categories', 'partners', $args );
}