<?php
// Creating the widget 
class partner_list_carousel_widget extends WP_Widget {
	
	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'partner_list_carousel_widget', 
		
		// Widget name will appear in UI
		__('Visa partners och sponsorer', 'partner_list_carousel_widget_domain'), 
		
		// Widget description
		array( 'description' => __( 'En widget som listar vald partnersgrupp', 'partner_list_carousel_widget_domain' ), ) 
		);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$a['type'] = $instance['type'];
		$a['group'] = $instance['groups'];
		$a['category'] = $instance['categories'];
		$a['size'] = $instance['size'];
		$a['height'] = $instance['height'];
		$a['links'] = $instance['links'];
		$a['controls'] = $instance['controls'];
		$a['title'] = $instance['showtitles']; 
		if ( isset($instance['spacing']) ) { $a['spacing'] = $instance['spacing']; } else { $a['spacing'] = 0; }
		if ( isset($instance['orderby']) ) { $a['orderby'] = $instance['orderby']; } else { $a['orderby'] = 'id'; }
		
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];
		 
		// This is where you run the code and display the output
		 
		if ( $a['group'] == "all" OR $a['group'] == 0 ) {
			$termArr = get_terms('partners-groups');
			foreach($termArr as $key => $termA) {
				$a['group'][$key] = $termA->term_id;
			}
		}
		
		if ( $a['category'] == "all" OR $a['category'] == 0 ) {
			$custom_terms = get_terms('partners-categories');
		} else {
			$custom_terms[0] = get_term_by( 'term_id', $a['category'], 'partners-categories' );
		}
		
		$getviewtype = partnerlist_views_get($a['type']);

		$dir = plugin_dir_path( __FILE__ );
		require $dir . "../views/" . $getviewtype['slug'] . ".php";
		
		//echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		// Check values
		if( $instance ) {
			$title = esc_attr($instance['title']);
			$type = esc_attr($instance['type']);
			$groups = esc_attr($instance['groups']);
			$categories = esc_attr($instance['categories']);
			$size = esc_attr($instance['size']);
			$height = esc_attr($instance['height']);
			$links = esc_attr($instance['links']);
			$controls = esc_attr($instance['controls']);
			$showtitles = esc_attr($instance['showtitles']); 
			$spacing = esc_attr($instance['spacing']);
			$orderby = esc_attr($instance['orderby']);
		} else {
			$title = __( 'Partnerlista', 'partner_list_carousel_widget_domain' );
			$type = __( 'carousel', 'partner_list_carousel_widget_domain' );
			$groups = __( 'all', 'partner_list_carousel_widget_domain' );
			$categories = __( 'all', 'partner_list_carousel_widget_domain' );
			$size = __( 'medium', 'partner_list_carousel_widget_domain' );
			$height = __( '300', 'partner_list_carousel_widget_domain' );
			$links = __( 'true', 'partner_list_carousel_widget_domain' );
			$controls = __( 'hide', 'partner_list_carousel_widget_domain' );
			$showtitles = __( 'show', 'partner_list_carousel_widget_domain' );
			$spacing = __( '0', 'partner_list_carousel_widget_domain' );
			$orderby = __( 'id', 'partner_list_carousel_widget_domain' );
		}
		
		partnerlist_add_widget_field("text", $this->get_field_id('title'), $this->get_field_name('title'), "Titel:", $title);
		
		$types = partnerlist_views();
		foreach ( $types as $key => $typ ) {
			$typeArr[$key][0] = $typ['name'];
			$typeArr[$key][1] = $typ['view_id'];
		}
		partnerlist_add_widget_field("select", $this->get_field_id('type'), $this->get_field_name('type'), "Typ:", $type, $typeArr);
		
		$terms = get_terms( 'partners-groups' );
		$groupsArr = Array(Array('Alla grupper', 0));
		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
			foreach ( $terms as $key => $term ) {
				$groupsArr[$key+1][0] = $term->name;
				$groupsArr[$key+1][1] = $term->term_id;
			}
		}
		partnerlist_add_widget_field("select", $this->get_field_id('groups'), $this->get_field_name('groups'), "Partnersgrupp att visa:", $groups, $groupsArr);
		
		$terms = get_terms( 'partners-categories' );
		$catsArr = Array(Array('Alla kategorier', 0));
		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
			foreach ( $terms as $key => $term ) {
				$catsArr[$key+1][0] = $term->name;
				$catsArr[$key+1][1] = $term->term_id;
			}
		}
		partnerlist_add_widget_field("select", $this->get_field_id('categories'), $this->get_field_name('categories'), "Partnerskategori att visa:", $categories, $catsArr);
		
		$sizes = get_intermediate_image_sizes();
		$sizeArr = Array(Array('full', 'full'));
		foreach ( $sizes as $key => $siz ) {
			$sizeArr[$key+1][0] = $siz;
			$sizeArr[$key+1][1] = $siz;
		}
		partnerlist_add_widget_field("select", $this->get_field_id('size'), $this->get_field_name('size'), "Storlek på bilderna:", $size, $sizeArr);
		
		partnerlist_add_widget_field("text", $this->get_field_id('height'), $this->get_field_name('height'), "Maxhöjd på logos (antal pixlar):", $height);
		
		$linkArr = Array(Array('Aktivera länkar', 'true'), Array('Inaktivera länkar', 'false'));
		partnerlist_add_widget_field("select", $this->get_field_id('links'), $this->get_field_name('links'), "Hantering av länkar:", $links, $linkArr);
		
		$controlArr = Array(Array('Dölj navigeringspilarna', 'hide'), Array('Visa navigeringspilarna', 'show'));
		//partnerlist_add_widget_field("select", $this->get_field_id('controls'), $this->get_field_name('controls'), "Hantering av navigeringspilar:", $controls, $controlArr);
		
		$titleArr = Array(Array('Visa partnertitel', 'show'), Array('Dölj partnertitel', 'hide'));
		partnerlist_add_widget_field("select", $this->get_field_id('showtitles'), $this->get_field_name('showtitles'), "Hantering av partnertitel:", $showtitles, $titleArr);
		
		partnerlist_add_widget_field("text", $this->get_field_id('spacing'), $this->get_field_name('spacing'), "Avstånd mellan bilder i scroller (antal pixlar):", $spacing);
		
		$orderbyArr = Array(Array('Ordna efter ID', 'id'), Array('Slumpvis ordning', 'rand'));
		partnerlist_add_widget_field("select", $this->get_field_id('orderby'), $this->get_field_name('orderby'), "Ordning av partners:", $orderby, $orderbyArr);
		
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['type'] = ( ! empty( $new_instance['type'] ) ) ? strip_tags( $new_instance['type'] ) : '';
		$instance['groups'] = ( ! empty( $new_instance['groups'] ) ) ? strip_tags( $new_instance['groups'] ) : '';
		$instance['categories'] = ( ! empty( $new_instance['categories'] ) ) ? strip_tags( $new_instance['categories'] ) : '';
		$instance['size'] = ( ! empty( $new_instance['size'] ) ) ? strip_tags( $new_instance['size'] ) : '';
		$instance['height'] = ( ! empty( $new_instance['height'] ) ) ? strip_tags( $new_instance['height'] ) : '';
		$instance['links'] = ( ! empty( $new_instance['links'] ) ) ? strip_tags( $new_instance['links'] ) : '';
		$instance['controls'] = ( ! empty( $new_instance['controls'] ) ) ? strip_tags( $new_instance['controls'] ) : '';
		$instance['showtitles'] = ( ! empty( $new_instance['showtitles'] ) ) ? strip_tags( $new_instance['showtitles'] ) : '';
		$instance['spacing'] = ( ! empty( $new_instance['spacing'] ) ) ? strip_tags( $new_instance['spacing'] ) : '';
		$instance['orderby'] = ( ! empty( $new_instance['orderby'] ) ) ? strip_tags( $new_instance['orderby'] ) : '';
		
		return $instance;
	}
} // Class partner_list_carousel_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'partner_list_carousel_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );
?>