<?php

// Register partnerlist_partners
function partnerlist_partners() {

	$labels = array(
		'name'                => _x( 'Partners', 'Post Type General Name', 'wordpress' ),
		'singular_name'       => _x( 'Partner', 'Post Type Singular Name', 'wordpress' ),
		'menu_name'           => __( 'Partners', 'wordpress' ),
		'parent_item_colon'   => __( 'Föräldrapartner:', 'wordpress' ),
		'all_items'           => __( 'Partners', 'wordpress' ),
		'view_item'           => __( 'Visa partners', 'wordpress' ),
		'add_new_item'        => __( 'Lägg till ny partner', 'wordpress' ),
		'add_new'             => __( 'Lägg till', 'wordpress' ),
		'edit_item'           => __( 'Redigera partner', 'wordpress' ),
		'update_item'         => __( 'Uppdatera partner', 'wordpress' ),
		'search_items'        => __( 'Sök partners', 'wordpress' ),
		'not_found'           => __( 'Ingen partner hittades', 'wordpress' ),
		'not_found_in_trash'  => __( 'Ingen partner hittades i papperskorgen', 'wordpress' ),
	);

	$rewrite = array(
		'slug'                => 'partner',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);

	$args = array(
		'label'               => __( 'partner', 'wordpress' ),
		'description'         => __( 'Används för att skapa hantera, lista och rotera partners och sponsorer.', 'wordpress' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'excerpt', 'thumbnail' ),
		'taxonomies'          => array( ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => 'partners',
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);

	register_post_type( 'partners', $args );
	flush_rewrite_rules();

}

// Hook into the 'init' action
add_action( 'init', 'partnerlist_partners', 0 );
