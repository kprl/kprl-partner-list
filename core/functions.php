<?php

add_action('wp_enqueue_scripts', 'callback_for_setting_up_scripts');
function callback_for_setting_up_scripts() {
	wp_register_style( 'bootstrapcss', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css' );
	wp_enqueue_style( 'bootstrapcss' );

	wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.3.1.slim.min.js' );
	wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js' );
	wp_enqueue_script( 'bootstrapjs', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js' );
}

/*** Hanterar de "views" som finns tillgängliga. ***/
function partnerlist_views() {
	global $partnerlist_views_array;
	ksort($partnerlist_views_array);
	return $partnerlist_views_array;
}

function partnerlist_views_add( $view_id, $name, $desc ) {
	global $partnerlist_views_array;
	$slug = sanitize_title($name);

	$partnerlist_views_array[$view_id]['view_id'] = $view_id;
	$partnerlist_views_array[$view_id]['name'] = $name;
	$partnerlist_views_array[$view_id]['slug'] = $slug;
	$partnerlist_views_array[$view_id]['desc'] = $desc;
}
function partnerlist_views_get( $input ) {
	global $partnerlist_views_array;

	foreach ( $partnerlist_views_array as $keysss => $plvA ) {
		if ($plvA['view_id'] == $input) {
			return $partnerlist_views_array[$keysss];
		}

		if ($plvA['slug'] == $input) {
			return $partnerlist_views_array[$keysss];
		}
	}
}

/*** Functions för att skapa och spara form-fält ***/
function partnerlist_add_meta_box_field($post_id, $metaattr = "partnerlist_value", $metatext = "Text", $type = "input", $value = NULL) {

	$partnerlist_value = get_post_meta( $post_id, $metaattr, true );

	echo "<div class='meta_input'>";

	echo '<label for="' . $metaattr . '">';
	_e( $metatext, 'wordpress' );
	echo '</label> ';

	if ($type == "input") {
		echo '<input type="text" id="' . $metaattr . '" name="' . $metaattr . '" value="' . esc_attr( $partnerlist_value ) . '" size="50" />';
	} else if ($type == "dtp") {
		echo '<script>
	$(function() {
		$( "#' . $metaattr . '" ).datepicker({dateFormat: "yy-mm-dd"});
	});
</script>';
		echo '<input type="text" id="' . $metaattr . '" name="' . $metaattr . '" value="' . esc_attr( date("Y-m-d", $partnerlist_value) ) . '" size="50" />';
	} else if ($type == "hidden") {
		echo '<input type="hidden" id="' . $metaattr . '" name="' . $metaattr . '" value="' . esc_attr( $partnerlist_value ) . '" size="50" />';
	} else if ($type == "text") {
		echo '<textarea id="' . $metaattr . '" name="' . $metaattr . '" cols="50" rows="3" />' . esc_textarea( $partnerlist_value ) . '</textarea>';
	} else if ($type == "chck") {
		echo "<input type='checkbox' id='" . $metaattr . "' name='" . $metaattr . "' " . checked( esc_attr( $partnerlist_value ), 1 ) . " value='1'>";
	} else if ($type == "radio") {
		echo "<input type='radio' id='" . $metaattr . "' name='" . $metaattr . "' " . checked( esc_attr( $partnerlist_value ), 1 ) . " value='1'>";
	} else if ($type == "select") {
		echo "<select name='" . $metaattr . "' id='" . $metaattr . "'>";
			foreach ($value as $val) { echo '<option value="' . $val[1] . '"' . selected( $partnerlist_value, $val[1], false ) . '>' . $val[0] . '</option>'; }
		echo '</select>';
	}

	echo "</div>";

}

function partnerlist_save_meta_box_field($post_id, $metaattr = "partnerlist_value", $type = "input", $time = "00:00:01") {

	// Make sure that it is set.
	if ( !isset( $_POST[$metaattr] ) ) {
		return;
	} else {

		if ($type == "input") {
			// Sanitize user input.
			$partnerlist_value = sanitize_text_field( $_POST[$metaattr] );
			update_post_meta( $post_id, $metaattr, $partnerlist_value );
		} else if ($type == "text") {
			update_post_meta(
			    $post_id,
			    $metaattr,
			    implode( "\n", array_map( 'sanitize_text_field', explode( "\n", $_POST[$metaattr] ) ) )
			);
		} else if ($type == "timestamp") {
			// Sanitize user input.
			$partnerlist_value = sanitize_text_field( $_POST[$metaattr] );
			update_post_meta( $post_id, $metaattr, strtotime($partnerlist_value . " " . $time) );
		}

	}

}

function partnerlist_add_widget_field($type = 'text', $id = NULL, $name = NULL, $text = NULL, $value = NULL, $input = NULL, $class = 'widefat') {

	if ($type == "text") {
		echo '<p>';
		echo '<label for="' . $id . '">' . _e($text, 'kprl-partner-list') . '</label>';
		echo '<input class="' . $class . '" id="' . $id . '" name="' . $name . '" type="' . $type . '" value="' . $value . '" />';
		echo '</p>';
	} else if ($type == "textarea") {
		echo '<p>';
		echo '<label for="' . $id . '">' . _e($text, 'kprl-partner-list') . '</label>';
		echo '<textarea id="' . $id . '" name="' . $name . '" class="' . $class . '">' . $value . '</textarea>';
		echo '</p>';
	} else if ($type == "checkbox") {
		echo '<p>';
		echo '<input class="' . $class . '" id="' . $id . '" name="' . $name . '" type="' . $type . '" value="1"' . checked( 1, $value, false ) . '/>';
		echo '<label for="' . $id . '">' . _e($text, 'kprl-partner-list') . '</label>';
		echo '</p>';
	} else if ($type == "select") {
		echo '<p>';
		echo '<label for="' . $id . '">' . _e($text, 'kprl-partner-list') . '</label>';
		echo '<select name="' . $name . '" id="' . $id . '" class="' . $class . '">';

		foreach ($input as $key => $option) {
			echo '<option value="' . $option[1]. '" id="' . $option[1] . '"', $value == $option[1] ? ' selected="selected"' : '', '>', $option[0], '</option>';
		}

		echo '</select>';
		echo '</p>';
	}

}

function get_custom_post_type_template( $single_template ) {
     global $post;

     if ($post->post_type == 'partners') {
          $single_template = dirname( __FILE__ ) . '/../single-kprl-partner-list.php';
     }
     return $single_template;
}
add_filter( 'single_template', 'get_custom_post_type_template' );

function my_custom_mime_types( $mimes ) {

	// New allowed mime types.
	$mimes['zip'] = 'application/zip';
	$mimes['tiff'] = 'image/tiff';
	$mimes['tif'] = 'image/tiff';
	$mimes['bmp'] = 'image/bmp';
	$mimes['svg'] = 'image/svg+xml';
	$mimes['psd'] = 'image/vnd.adobe.photoshop';
	$mimes['ai'] = 'application/postscript';
	//$mimes['indd'] = 'application/x-indesign'; // not official, but might still work
	$mimes['eps'] = 'application/postscript';

	// Optional. Remove a mime type.
	unset( $mimes['exe'] );
	return $mimes;
}
add_filter('upload_mimes', 'my_custom_mime_types');
