<?php

/*** För en gemensam menykategori i adminlägets vänsterspalt. ***/
function partners_add_admin_menu() {

    add_menu_page(
    	__('Partners', 'wordpress'),
    	__('Partners', 'wordpress'),
    	'manage_options',
    	'partners',
    	'',
    	'dashicons-groups',
    	34
    	);

	add_submenu_page(
		'partners',
		__('Partnerskategorier','wordpress'),
		__('Partnerskategorier','wordpress'),
		'manage_options',
		'/edit-tags.php?taxonomy=partners-categories'
		);

	add_submenu_page(
		'partners',
		__('Partnersgrupper','wordpress'),
		__('Partnersgrupper','wordpress'),
		'manage_options',
		'/edit-tags.php?taxonomy=partners-groups'
		);

	// highlight the proper top level menu
	function partners_menu_correction($parent_file) {
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ($taxonomy == 'partners-categories' || $taxonomy == 'partners-groups')
			$parent_file = 'partners';
		return $parent_file;
	}
	add_action('parent_file', 'partners_menu_correction');

}
add_action( 'admin_menu', 'partners_add_admin_menu' );
